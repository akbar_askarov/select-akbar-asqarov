-- 1st task:

SELECT
    sr.store_id,
    sr.full_name
FROM (
    SELECT
        st.store_id,
        sf.staff_id,
        sf.first_name || ' ' || sf.last_name AS full_name,
        SUM(p.amount) AS total_revenue
    FROM
        store st
    JOIN staff sf ON sf.store_id = st.store_id
    JOIN payment p ON p.staff_id = sf.staff_id
    WHERE
        EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY
        st.store_id, sf.staff_id
) sr
JOIN (
    SELECT
        store_id,
        MAX(total_revenue) AS max_revenue
    FROM (
        SELECT
            st.store_id,
            sf.staff_id,
            SUM(p.amount) AS total_revenue
        FROM
            store st
        JOIN staff sf ON sf.store_id = st.store_id
        JOIN payment p ON p.staff_id = sf.staff_id
        WHERE
            EXTRACT(YEAR FROM p.payment_date) = 2017
        GROUP BY
            st.store_id, sf.staff_id
    ) staff_revenue
    GROUP BY
        store_id
) mr
ON sr.store_id = mr.store_id
WHERE
    sr.total_revenue = mr.max_revenue;

-- 2nd task:

SELECT
  f.title,
  COUNT(*) AS rental_count,
  f.rating
FROM
  film f
INNER JOIN inventory i ON f.film_id = i.film_id
INNER JOIN rental r ON i.inventory_id = r.inventory_id
GROUP BY
  f.title, f.rating
ORDER BY
  rental_count DESC
LIMIT 5;

-- 3rd task:

SELECT a.first_name, a.last_name, subquery.last_casting_year
FROM actor a
JOIN (
  SELECT fa.actor_id, MAX(f.release_year) AS last_casting_year
  FROM film_actor fa
  JOIN film f ON f.film_id = fa.film_id
  GROUP BY fa.actor_id
) subquery ON a.actor_id = subquery.actor_id
ORDER BY last_casting_year ASC;